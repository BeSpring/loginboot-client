package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.example.demo.client.LoginClient;

@Configuration
public class LoginConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("login.wsdl");
		return marshaller;
	}
	
	@Bean
	public LoginClient loginClient(Jaxb2Marshaller marshaller) {
		LoginClient client = new LoginClient();
		client.setDefaultUri("http://localhost:8080/login");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}
