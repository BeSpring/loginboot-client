package com.example.demo.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import login.wsdl.GetLoginRequest;
import login.wsdl.GetLoginResponse;

public class LoginClient extends WebServiceGatewaySupport{
	
	private static final Logger log = LoggerFactory.getLogger(LoginClient.class);
	
	public GetLoginResponse getLogin(String user,String password) {

		GetLoginRequest request = new GetLoginRequest();
		
		request.setUser(user);
		request.setPassword(password);

		log.info("Requesting location for " + user);

		GetLoginResponse response = (GetLoginResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://localhost:8080/ws/login", request,
						new SoapActionCallback(
								"http://spring.io/guides/gs-producing-web-service/GetCountryRequest"));

		return response;
	}
}
