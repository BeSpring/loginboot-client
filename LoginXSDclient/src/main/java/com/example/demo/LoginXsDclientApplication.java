package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.demo.client.LoginClient;

import login.wsdl.GetLoginResponse;

@SpringBootApplication
public class LoginXsDclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginXsDclientApplication.class, args);
	}
	
	@Bean
	CommandLineRunner lookup(LoginClient login) {
		String user="fra";
		String pass="fra";
		GetLoginResponse response = login.getLogin(user, pass);
		boolean x = response.getControllo().isControllo();
		System.err.println(x);
		return null;
	
	}

}

