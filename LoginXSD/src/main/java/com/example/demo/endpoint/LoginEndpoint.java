package com.example.demo.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.example.demo.login.Controllo;
import com.example.demo.login.GetLoginRequest;
import com.example.demo.login.GetLoginResponse;
import com.example.demo.repository.UtentiRepository;

@Endpoint
public class LoginEndpoint {

	private static final String NAMESPACE_URI = "http://example.com/demo/login";

	private UtentiRepository utentiRepository;

	@Autowired
	public LoginEndpoint(UtentiRepository utentiRepository) {
		this.utentiRepository = utentiRepository;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLoginRequest")
	@ResponsePayload
	public GetLoginResponse getLogin(@RequestPayload GetLoginRequest request ) {
		GetLoginResponse response = new GetLoginResponse();
		Controllo x = new Controllo();
		x.setControllo(utentiRepository.controllo(request.getUser(),request.getPassword()));
		response.setControllo(x);
		
		return response;	
	}

}
