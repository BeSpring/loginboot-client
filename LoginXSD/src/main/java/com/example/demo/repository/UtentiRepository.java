package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import com.example.demo.login.Utente;

@Repository
public class UtentiRepository {

	List<Utente> utenti = new ArrayList<>();
	
	@PostConstruct
	public void initData() {
		 
		Utente primo = new Utente();
		 primo.setUser("fra");
		 primo.setPassword("fra");
		 utenti.add(primo);
		 
		 Utente secondo = new Utente();
		 secondo.setUser("luca");
		 secondo.setPassword("luca");
		 utenti.add(secondo);
		 
	}
	
	public boolean controllo(String user,String password) {
		
		for(int i=0;i<=utenti.size()-1;i++) {
			if(utenti.get(i).getUser().equals(user)&&utenti.get(i).getPassword().equals(password)) {
				return true;
			}
		}
		
		return false;
	}
}
